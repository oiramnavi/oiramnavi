<?php
 // Operators
// Comparison Operators
$x = 100;
$y = 100;
echo "x = $x <br>";
echo "y = $y <br><br>";

// equal
var_dump($x == $y); // bool(true)
echo "x == $y <br>";

$y = 10;
var_dump($x == $y); // bool(false)
echo "x == $y <br><br>";

// Identical
var_dump($x === $y); // bool(false)
echo "x === $y<br>";

$y = 100;
var_dump($x === $y); // bool(true)
echo "x === $y<br><br>";

// Not equal
var_dump($x != $y); // bool(false)
echo "x != $y <br>";

$y = 10;
var_dump($x != $y); // fool(true)
echo "x != $y <br><br>";

// Not equal
var_dump($x <> $y); // fool(true)
echo "x <> $y <br>";

$y = 100;
var_dump($x <> $y); // returns false because values are equal
echo "x <> $y <br><br>";
?>