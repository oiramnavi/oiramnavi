<?php
/*
    An integer must have at least one digit
    An integer must not have a decimal point
    An integer can be either positive or negative
    Integers can be specified in three formats: 
    decimal (10-based), hexadecimal (16-based - prefixed with 0x) or octal (8-based - prefixed with 0)
 */
//In the following example $x is an integer. The PHP var_dump() function returns the data type and value:

$x = 2147483647;  // This is the bigest integer number 
$y = 2147483648; // And this is for one number larger then integer i zove se float
var_dump($x);
echo "<br>";
var_dump($y);
?>