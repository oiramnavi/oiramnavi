<?php
/*
PHP String
A string can be any text inside quotes. 
You can use single or double quotes
*/
$x = "Hello with double quotes";
$y = 'Hello with single quotes';

echo $x;
echo "<br>";
echo $y;
?>