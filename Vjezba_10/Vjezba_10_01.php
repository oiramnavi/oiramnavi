<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vjezba_10_01</title>
</head>
<?php
 // while Loops
// PHP while loops execute a block of code while the specified condition is true.
// Loops = Often when you write code, you want the same block of code to run over and over again in a row.
// while Loop
$x = 1;

while($x <= 5)
	{
		echo "The number is: $x <br>";
		$x++;
	}
?>
<body>
</body>
</html>