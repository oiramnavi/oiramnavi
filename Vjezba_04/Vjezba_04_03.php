<?php
/*
 PHP Float
 	A float (floating point number) is a number with a decimal point or a number in exponential form.
	In the following example $x is a float. The PHP var_dump() function returns the data type and value:
*/
$x = 15498.3165465466;
$y = 18.3165466;
$c = 1.3;
$v = 15498488868.316;
var_dump($x);
echo "<br>";
var_dump($y);
echo "<br>";
var_dump($c);
echo "<br>";
var_dump($v);
?>