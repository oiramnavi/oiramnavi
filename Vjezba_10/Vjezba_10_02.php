<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<?php
 // do while Loops
// The do...while loop will always execute the block of code once, it will then check the condition, and repeat the loop while the specified condition is true.
echo "Do while Loops <br>";
$x = 1;

do 
	{
		echo "The number is: $x <br>";
		$x++;
	}
	while ($x <= 5);
echo "<br><br>";
// Notice that in a do while loop the condition is tested AFTER executing the statements within the loop. This means that the do while loop would execute its statements at least once, even if the condition is false the first time.
$x = 6;

do 
	{
		echo "The number is: $x <br>";
		$x++;
	}
	while ($x <=5);
?>
<body>
</body>
</html>