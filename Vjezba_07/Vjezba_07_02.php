<?php
 // Operators
// Assignment Operators
echo "x = 10 <br>";
$x = 10;
echo "$x <br><br>";

// Addition
echo "x = 20 <br> x += 100 <br>";
$x = 20;
$x += 100;
echo "$x <br><br>";

// Subtraction
echo "x -= 20<br>";
$x -= 20;
echo "$x <br><br>";

// Multiplication
echo "x *= 2 <br>";
$x *= 2;
echo "$x <br><br>";

// Division
echo "x /= 3<br>";
$x /= 3;
echo "$x <br><br>";

// Modulus
echo "x %= 5<br>";
$x %= 5;
echo "$x";
?>