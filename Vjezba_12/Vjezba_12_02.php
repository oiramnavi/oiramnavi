<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vjezba_12_02</title>
</head>
<?php
// Function Arguments
function familyName($fname, $year)
	{
		echo "$fname Refsnes. Born in $year <br>";
	}
familyName("Atlija", "1993");
familyName("Lepan",  "1992");
familyName("Žgela",  "1988");
familyName("Fintić", "1992");
familyName("Šuvak",  "1992");
?>
<body>
</body>
</html>