<?php
echo "Hello PHP World <br>";
echo "This is second row <br><br>";

// PHP automatically converts the variable to the correct data type, depending on its value.
$a = 4;
$b = 3;
$c = $a + $b;
echo "a = $a <br>";
echo "b = $b <br>"; 
echo "a + b = $c <br><br>";

// PHP variable names are case-sensitive!
$color = "red";
$COLOR = "BLACK";
echo "I have $color car, and $COLOR bike <br><br>";

//Output Variables
$txt = "PHP";
echo "I want to know $txt <br>";
//or
echo "I want to know " . $txt . "<br>";
?>