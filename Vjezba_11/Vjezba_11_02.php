<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vjezba_11_02</title>
</head>
<?php
 // foreach Loop
// The foreach loop works only on arrays, and is used to loop through each key/value pair in an array.
$colors = array("red", "green", "blue", "yellow");
foreach ($colors as $value)
	{
		echo "$value <br>";
	}
?>
<body>
</body>
</html>