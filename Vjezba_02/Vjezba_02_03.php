<?php

$x = 6;
$y = 9;

function myTest()
	{
		global $x, $y; // Without (globa) variable, program will generate an error. Because function do not have a value for (x and y)  so we used global variable.
		$y = $x + $y;
	}

myTest(); // run function
echo $y; // outputs 15

?>