<?php
//A variable declared outside a function has a GLOBAL SCOPE and can only be accessed outside a function:
$x = 4; // Global scope

function myTest() 
	{
		// using x inside this function will generate an erroe.
		echo "<p> Variable x outside function is: $x</p>";
	}
myTest();
echo "<p>Variable x outside function is: $x</p>";
?>