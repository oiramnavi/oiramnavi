<?php
 // Operators
// Arithmetic Operators
$x = 5;
$y = 2;
echo "x = 5 <br>";
echo "y = 2 <br><br>";

// Addition
$a = $x + $y;
echo "x + y = $a <br><br>";

// Subtraction
$s = $x - $y;
echo "x - y = $s <br><br>";

// Multiplication
$m = $x * $y;
echo "x * y = $m <br><br>";

// Division
$d = $x / $y;
echo "x / y = $d <br><br>";

// Modulus
$mo = $x % $y;
echo "x % y = $mo<br><br>";

// Exponentiation
$e = $x **$y;
echo "x ** y = $e";

?>