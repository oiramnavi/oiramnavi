<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vjezba_11_01</title>
</head>
<?php
  // for Loops
 // PHP for loops execute a block of code a specified number of times.
//The for loop is used when you know in advance how many times the script should run.
for ($x = 0; $x <= 20; $x++)
	{
		echo "The number is: $x <br>";
	}
?>
<body>
</body>
</html>