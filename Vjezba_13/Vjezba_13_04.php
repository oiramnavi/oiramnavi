<?php
 // Associative Arraya
// Associative arrays are arrays that use named keys that you assign to them.
$albums = array("Arch Enemy"=>"10", "Amon amarth"=>"10", "In Flames"=>"12");

echo "Arch Enemy have " . $albums['Arch Enemy'] . " Albums.";
?>